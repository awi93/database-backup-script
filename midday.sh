#!/bin/bash

logger "Running Automated Backup - Midnight"
now=`date +%Y%m%d`
prev=`date -d "7 days ago" +%Y%m%d`

base_path="/opt/backup"
dump_fpath="$base_path/day_backup_db-$now.sql.gz"
prev_fname="day_backup_db-$prev.sql.gz"


logger "Creating Backup for $now. Gzip to $dump_fpath"
pg_dump -U nasker_api_db -h localhost -p 5488 nasker_db | gzip -9 > $dump_fpath

logger "Uploading Backup File to Gdrive."
gdrive upload $dump_fpath

logger "Removing Local Backup File"
rm $dump_fpath

logger "Getting File ID for prev. backup file. Filename=$prev_fname"
id=`gdrive list | grep $prev_fname | cut -d ' ' -f 1`
if [ "$id" != "" ]; then
    logger "Prev. backup file is found with id=$id"
    gdrive delete $id;
else
    logger "Prev. backup file is not found"
fi
logger "Automated Backup - Midnight Finished"